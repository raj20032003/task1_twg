from django.urls import path
from api.views.GeneralResisterViews import ListGeneralRegister, CreateGeneralRegister, UpdateGeneralRegister, DeleteGeneralRegister, GeneralRegisterDetailView

urlpatterns = [
    path('', ListGeneralRegister.as_view()),
    path('create', CreateGeneralRegister.as_view()),
    path('update/<str:pk>/', UpdateGeneralRegister.as_view()),
    path('delete/<str:pk>/', DeleteGeneralRegister.as_view()),
    path('gr/<str:pk>/', GeneralRegisterDetailView.as_view()),
]

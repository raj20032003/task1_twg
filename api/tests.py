from django.test import TestCase
from api.models.GeneralRegisterModels import GeneralRegister, StudentEnrollment, IncomeTypes, Incomes


class GeneralRegisterTestCase(TestCase):

    def setUp(self):
        self.person1 = GeneralRegister()
        self.person1.name_of_student="Ali"
        self.person1.father_name="Ahmed"
        self.person1.gender="M"
        self.person1.caste="Memon"
        self.person1.religion="Islam"
        self.person1.address="models.CharField(max_length=256)"
        self.person1.date_of_birth="2022-02-21"
        self.person1.place_of_birth="models.CharField(max_length=256)"
        self.person1.last_institution_attended="models.CharField(max_length=256)"
        self.person1.date_of_admission="2022-02-21"
        self.person1.admitted_in_class=3
        self.person1.date_of_removal="2022-02-21"
        self.person1.class_at_time_of_removal=4
        self.person1.cause_of_removal="models.CharField(max_length=256)"
        self.person1.remarks="models.CharField(max_length=1024)"
        self.person1.save()

    def test_str_method(self):
        self.assertEqual(self.person1.__str__(), "Ali")
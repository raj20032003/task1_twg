from django.contrib import admin
from api.models.GeneralRegisterModels import GeneralRegister, StudentEnrollment, Incomes, IncomeTypes

admin.site.register(GeneralRegister)
admin.site.register(StudentEnrollment)
admin.site.register(Incomes)
admin.site.register(IncomeTypes)

from django.db import models


class RoleModel(models.Model):
    title = models.CharField(max_length=256)
    remarks = models.CharField(max_length=1024)

    def __str__(self):
        return self.title.__str__()
